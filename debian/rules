#!/usr/bin/make -f
# -*- makefile -*-
# debian/rules file for beast-mcmc
# Andreas Tille <tille@debian.org>
# GPL

#export DH_VERBOSE=1

export LC_ALL=C.UTF-8

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

JAVA_HOME  := /usr/lib/jvm/default-java

DEBJAR    := /usr/share/java
CLASS_PATH := /usr/share/ant/lib/ant.jar:/usr/share/ant/lib/ant-launcher.jar:/usr/share/ant/lib/ant-nodeps.jar:/usr/share/ant/lib/ant-junit.jar:$(JAVA_HOME)/lib/tools.jar:$(DEBJAR)/beagle.jar:$(DEBJAR)/mpj.jar:$(DEBJAR)/org.boehn.kmlframework.jar:$(DEBJAR)/itext1-1.4.jar:$(DEBJAR)/junit4.jar:$(DEBJAR)/figtree.jar:lib/colt.jar:$(DEBJAR)/options.jar:$(DEBJAR)/mtj.jar:$(DEBJAR)/jam.jar:$(DEBJAR)/jdom1.jar:$(DEBJAR)/jebl.jar:$(DEBJAR)/commons-math.jar:$(DEBJAR)/EJML.jar:$(DEBJAR)/jlapack-lapack.jar:/usr/lib/R/site-library/rJava/jri/JRI.jar

# to run the test suite
JAVA       := $(JAVA_HOME)/bin/java
ANT_HOME   := /usr/share/ant
ANT_BIN    := $(ANT_HOME)/bin/ant
ANT_ARGS   := -Dcompile.debug=true -Dcompile.optimize=true

%:
	dh $@ --with javahelper

override_dh_auto_clean:
	rm -f native/*\.o native/*\.so native/*\.so\.*
	rm -rf release/Linux/B*
ifeq ($(HASTEXDOC),yes)
	for cleantex in \
		    Practical_BEAST \
		    BookSection \
		    Practical_BEAST \
		    Yule \
		    SerialSampleCoalescent \
		    BEAST14_Manual \
		; do \
    	    find doc -not -name "*.tex" -a -not -name "*.bib" -and -name "$${cleantex}.*" -exec rm -f \{\} \;  ; \
    	done
endif
	ant clean

CFLAGS:=$(shell dpkg-buildflags --get CFLAGS)
LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS)
PDFLATEX:=pdflatex -interaction=batchmode

override_dh_auto_build:
	# CLASSPATH=$(CLASS_PATH)
	mkdir -p lib
	ant build
	# FIXME: There is no such file build_tracer.xml any more
	# ant -buildfile build_tracer.xml dist
	# native/compileNativeLinux.sh does not create dynamic libraries
	cd native ; $(MAKE) -f Makefile.linux JAVA_HOME=$(JAVA_HOME)
#		gcc $(CFLAGS) $(LDFLAGS) -Wall -funroll-loops -ffast-math -fstrict-aliasing -c -I/usr/lib/jvm/default-java/include/ NucleotideLikelihoodCore.c -o libNucleotideLikelihoodCore.o ; \
#		gcc -shared -Wl,-soname,libNucleotideLikelihoodCore.so.0 -o libNucleotideLikelihoodCore.so.0 libNucleotideLikelihoodCore.o -lc ; \
#		gcc $(CFLAGS) $(LDFLAGS) -Wall -funroll-loops -ffast-math -fstrict-aliasing -c -I/usr/lib/jvm/default-java/include/ AminoAcidLikelihoodCore.c -o libAminoAcidLikelihoodCore.o ; \
#		gcc -shared -Wl,-soname,libAminoAcidLikelihoodCore.so.0 -o libAminoAcidLikelihoodCore.so libAminoAcidLikelihoodCore.o -lc
ifeq ($(HASTEXDOC),yes)
	#
	# Build extra LaTeX documentation
	#
	cd doc/BookSection; $(PDFLATEX) BookSection.tex; $(PDFLATEX) BookSection.tex
	cd doc/tutorial/Virus_Practical*; $(PDFLATEX) Practical_BEAST.tex; $(PDFLATEX) Practical_BEAST.tex
	cd doc; $(PDFLATEX) Yule.tex; $(PDFLATEX) Yule.tex
	cd doc; $(PDFLATEX) SerialSampleCoalescent.tex; $(PDFLATEX) SerialSampleCoalescent.tex
	cd doc; $(PDFLATEX) BEAST14_Manual.tex; $(PDFLATEX) BEAST14_Manual.tex
endif

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	#####################################################################
	## Run test suite                                                  ##
	## --------------                                                  ##
	## While this test suite runs quite a bit of time (about 30min) it ##
	## is recommended to be run at least once per new version upload   ##
	#####################################################################
	#
	# $(JAVA) -classpath $(CLASS_PATH) -Dant.home=$(ANT_HOME) org.apache.tools.ant.Main $(ANT_ARGS) -buildfile build.xml junit
endif
